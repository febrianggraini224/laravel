@extends('pelanggans.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Laravel 9 CRUD Example from scratch - ItSolutionStuff.com</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('pelanggans.create') }}">Create New Pelanggan</a>
        </div>
    </div>
</div>

@if(auth()->check())
    <form action="{{ route('logout') }}" method="post">
        @csrf
        <button type="submit">Logout</button>
    </form>
@else
    <a href="{{ route('login') }}">Login</a>
    <a href="{{ route('register') }}">Register</a>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Jekel</th>
        <th>Email</th>
        <th>Hp</th>
        <th>Alamat</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($pelanggans as $pelanggan)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $pelanggan->name }}</td>
        <td>{{ $pelanggan->jekel }}</td>
        <td>{{ $pelanggan->email }}</td>
        <td>{{ $pelanggan->hp }}</td>
        <td>{{ $pelanggan->alamat }}</td>
        <td>
            <form action="{{ route('pelanggans.destroy', $pelanggan->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('pelanggans.show', $pelanggan->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('pelanggans.edit', $pelanggan->id) }}">Edit</a>
                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $pelanggans->links() !!}
@endsection
