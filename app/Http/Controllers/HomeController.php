<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function update_password(){
    $user= auth::user();
    return view ('update_password',compact('user'));
    }

    public function store_password(Request $request){
        $request->validate([
            'new_password'=>'required|8|confirmed'
        ]);
        


    }
}